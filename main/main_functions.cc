/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "main_functions.h"

#include "detection_responder.h"
#include "image_provider.h"
#include "model_settings.h"
#include "person_detect_model_data.h"
#include "tensorflow/lite/micro/kernels/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/version.h"

// Globals, used for compatibility with Arduino-style sketches.
namespace {
tflite::ErrorReporter* error_reporter = nullptr;
const tflite::Model* model = nullptr;
tflite::MicroInterpreter* interpreter = nullptr;
TfLiteTensor* input = nullptr;
TfLiteTensor* output = nullptr;

// An area of memory to use for input, output, and intermediate arrays.
constexpr int kTensorArenaSize = 144 * 1024;
static uint8_t tensor_arena[kTensorArenaSize];
}  // namespace

// The name of this function is important for Arduino compatibility.
void setup() {
  // Set up logging. Google style is to avoid globals or statics because of
  // lifetime uncertainty, but since this has a trivial destructor it's okay.
  // NOLINTNEXTLINE(runtime-global-variables)
  static tflite::MicroErrorReporter micro_error_reporter;
  error_reporter = &micro_error_reporter;

  // Map the model into a usable data structure. This doesn't involve any
  // copying or parsing, it's a very lightweight operation.
  model = tflite::GetModel(g_person_detect_model_data);
  if (model->version() != TFLITE_SCHEMA_VERSION) {
    TF_LITE_REPORT_ERROR(error_reporter,
                         "Model provided is schema version %d not equal "
                         "to supported version %d.",
                         model->version(), TFLITE_SCHEMA_VERSION);
    return;
  }

  // Pull in only the operation implementations we need.
  // This relies on a complete list of all the ops needed by this graph.
  // An easier approach is to just use the AllOpsResolver, but this will
  // incur some penalty in code space for op implementations that are not
  // needed by this graph.
  //
  // NOLINTNEXTLINE(runtime-global-variables)
  tflite::ops::micro::AllOpsResolver resolver;

  // Build an interpreter to run the model with.
  static tflite::MicroInterpreter static_interpreter(
      model, resolver, tensor_arena, kTensorArenaSize, error_reporter);
  interpreter = &static_interpreter;

  // Allocate memory from the tensor_arena for the model's tensors.
  TfLiteStatus allocate_status = interpreter->AllocateTensors();
  if (allocate_status != kTfLiteOk) {
    TF_LITE_REPORT_ERROR(error_reporter, "AllocateTensors() failed");
    return;
  }

  // Get information about the memory area to use for the model's input.
  input = interpreter->input(0);
  TFLITE_CHECK_NE(nullptr, input);
  TF_LITE_REPORT_ERROR(error_reporter, "input ptr not null");
  TF_LITE_REPORT_ERROR(error_reporter, "input dims size %d", input->dims->size);
  TF_LITE_REPORT_ERROR(error_reporter, "input dims data0 %d", input->dims->data[0]);
  TF_LITE_REPORT_ERROR(error_reporter, "input dims data1 %d", input->dims->data[1]);
  TF_LITE_REPORT_ERROR(error_reporter, "input dims data2 %d", input->dims->data[2]);
  TF_LITE_REPORT_ERROR(error_reporter, "input dims data3 %d", input->dims->data[3]);
  TF_LITE_REPORT_ERROR(error_reporter, "input type %d", input->type);
  TF_LITE_REPORT_ERROR(error_reporter, "kTfLiteFloat32 type %d", kTfLiteFloat32);
  TFLITE_CHECK_EQ(4, input->dims->size);
  TFLITE_CHECK_EQ(1, input->dims->data[0]);
  TFLITE_CHECK_EQ(kNumRows, input->dims->data[1]);
  TFLITE_CHECK_EQ(kNumCols, input->dims->data[2]);
  TFLITE_CHECK_EQ(kNumChannels, input->dims->data[3]);
  TFLITE_CHECK_EQ(kTfLiteInt8, input->type);
  TF_LITE_REPORT_ERROR(error_reporter, ">>>>>>>>> input checks OK!");
  
  output = interpreter->output(0);
  TFLITE_CHECK_NE(nullptr, output);
  TF_LITE_REPORT_ERROR(error_reporter, "output ptr not null");
  TF_LITE_REPORT_ERROR(error_reporter, "output dims size %d", output->dims->size);
  TF_LITE_REPORT_ERROR(error_reporter, "output dims data0 %d", output->dims->data[0]);
  TF_LITE_REPORT_ERROR(error_reporter, "output dims data1 %d", output->dims->data[1]);
  TF_LITE_REPORT_ERROR(error_reporter, "output type %d", output->type);
  TFLITE_CHECK_EQ(2, output->dims->size);
  TFLITE_CHECK_EQ(1, output->dims->data[0]);
  TFLITE_CHECK_EQ(3, output->dims->data[1]);
  TFLITE_CHECK_EQ(kTfLiteInt8, output->type);
  TF_LITE_REPORT_ERROR(error_reporter, ">>>>>>>>> output checks OK!");
}

// The name of this function is important for Arduino compatibility.
void loop() {
  // Get image from provider.
  if (kTfLiteOk != GetImage(error_reporter, kNumCols, kNumRows, kNumChannels,
                            input->data.int8)) {
    TF_LITE_REPORT_ERROR(error_reporter, "Image capture failed.");
  }

  // Run the model on this input and make sure it succeeds.
  if (kTfLiteOk != interpreter->Invoke()) {
    TF_LITE_REPORT_ERROR(error_reporter, "Invoke failed.");
  }

  // Process the inference results.
  int8_t usb_score = output->data.int8[kLeftIndex];
  int8_t none_score = output->data.int8[kNoneIndex];
  int8_t ieee_score = output->data.int8[kRightIndex];

  if ( ( usb_score > none_score ) && ( usb_score > ieee_score ) )
  {
      printf("%s \n", kCategoryLabels[kLeftIndex]);
  }
  else if ( ieee_score > none_score )
  {
      printf("%s \n", kCategoryLabels[kRightIndex]);
  }
  else
  {
      printf("%s \n", kCategoryLabels[kNoneIndex]);
  }
  
  //RespondToDetection(error_reporter, usb_score, none_score, ieee_score);
}
